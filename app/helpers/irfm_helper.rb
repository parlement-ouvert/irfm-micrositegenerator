module IrfmHelper

  require 'net/http'
  require 'uri'
  require 'json'

  def launch_gitlab_trigger(an_username, accounting_username, accounting_password)

    private_token = ENV["PRIVATE_TOKEN"]

    # Fetching project ID
    puts "Fetiching project ID"
    url_project = "https://framagit.org/api/v4/projects/irfm%2Firfm-#{an_username}-script"
    uri = URI.parse(url_project)
    request = Net::HTTP::Get.new(uri)
    request["Private-Token"] = private_token

    req_options = {
        use_ssl: uri.scheme == "https",
    }

    response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
      http.request(request)
    end

    unless response.code == "200"
      puts response.code
      puts response.body
      return { result: 'KO', message: response.body }
    end

    id_project = JSON.parse(response.body)['id']
    puts "ID PROJECT : #{id_project}"

    # Fetching trigger
    puts "Fetching trigger token"
    url_trigger = "https://framagit.org/api/v4/projects/#{id_project.to_s}/triggers"
    uri = URI.parse(url_trigger)
    request = Net::HTTP::Get.new(uri)
    request["Private-Token"] = private_token

    req_options = {
        use_ssl: uri.scheme == "https",
    }

    response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
      http.request(request)
    end

    unless response.code == "200"
      puts response.code
      puts response.body
      return {result: 'KO', message: response.body }
    end

    trigger_token = JSON.parse(response.body).first['token']
    puts "TRIGGER TOKEN : #{trigger_token}"

    # Launching CI
    puts "Launching CI"
    url_ci = "https://framagit.org/api/v4/projects/#{id_project.to_s}/trigger/pipeline"
    uri = URI.parse(url_ci)
    request = Net::HTTP::Post.new(uri)

    req_options = {
        use_ssl: uri.scheme == "https",
    }

    request.set_form_data({
                              "ref" => "master",
                              "token" => trigger_token,
                              "variables[USERNAME]" => accounting_username,
                              "variables[PASSWORD]" => accounting_password
                          })

    response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
      http.request(request)
    end

    unless response.code == "200" || response.code == "201"
      puts response.code
      puts response.body
      return {result: 'KO', message: response.body }
    end

    puts response.body
    return {result: 'OK', message: response.body }

  end

end