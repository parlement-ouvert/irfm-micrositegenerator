class HomeController < ApplicationController
  skip_before_action :verify_authenticity_token
  include IrfmHelper

  def main

  end

  def check
    an_account = params[:inputAssnatmail].split('@').first

    @res = launch_gitlab_trigger(an_account, params[:inputBanklogin], params[:inputBankpass])

    @link = "https://framagit.org/irfm/irfm-#{an_account}-script/pipelines"

    if @res[:result] == 'OK'
      render 'check_ok'
    else
      flash[:alert] = "ERREUR : "+@res[:message].to_s
      render 'main'
    end
  end

end